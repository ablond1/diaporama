# Cats
![Chilling cat](https://upload.wikimedia.org/wikipedia/commons/1/15/Cat_August_2010-4.jpg)
---
## Habits
- Rest
- Hunting
---
## Food
### Healty
1. Chicken
2. Pork
3. Beef
----
### To avoid
1. Chocolate
2. Too much milk
3. White sugar
---
## Funny video
<iframe src="https://www.youtube.com/embed/RsSVGV6RKvg?si=73E3OFTP31TmTNQn"></iframe>
----
|  Male  | Female |
| ------ | ------ |
| Edgar  |Georgia |
| Gaspar |  Luna  |
